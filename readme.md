## RAII 

We briefly study the **RAII** concept (Resource Acquisition 
Is Initialization).

> RAII is a misnomer. It really has nothing to do with
> _resource acquisition_, instead it is about the 
> safe realease of resources. 

Perhaps a better name (that hasn't caught up yet) is
**SBRM**, _Scope-Based Resource Management_. Here, the
word _resource_ doesn't just mean memory. It refers to
things that we have only a finite supply of and we
should pay special attention to their release. For
example, it could refer to file handles, network
sockets, etc.


### So how does this RAII work?

In short, the idea is to represent a resource by a local
object, so that the local object's destructor will release
the resource.

For example, consider:

```c++
class File_handle {
  private:
    FILE* p;

  public:
    File_handle(const char* n, const char* a){
      p = fopen(n,a);
      if ( p == 0 )
        throw Open_error(errno);
    }
	 
    File_handle(FILE* pp){
      p = pp;
      if ( p == 0 )
        throw Open_error(errno);
    }

    ~File_handle(){
      fclose(p);
    }

    operator FILE*(){
      return p;
    }

    // ...
};

void f(const char* fn){
  File_handle f(fn,"rw");         // open fn for reading and writing
  // use file through f

  // When File_handle goes out of scope the destructor
  // calls fclose(), releasing the resource.
}

```

### Ok but... how do I RAII???

In short, make sure you follow these steps

* encapsulate each reasource into a class, where

    - the constructor acquires the resource and
      throws an exception if this cannot be done
    - the destructor releases the resource and
      never throws exceptions

* always use the resource via an instance of a 
   RAII-class that either

    - has temporary lifetime itself, or
    - has lifetime that is bounded to the lifetime
      of a temporary object



