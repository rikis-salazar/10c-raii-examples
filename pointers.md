### What is an `auto_ptr` ?

An `auto_ptr` is an example of very simple handle class, defined in
`<memory>`, supporting exception safety using the **RAII** technique.
An `auto_ptr` _holds_ a pointer, _can be used as_ a pointer, and 
_deletes_ the object pointed to at the end of its scope. 

For example:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp} 
	#include<memory>
	using namespace std;

	struct X {
		int m;
		// OTHER FIELDS AND FUNCTIONS
	};

	void some_function(){
		// Request resource via RAII object
		auto_ptr<X> p(new X);

		// Request resource 'by hand' 
		X* q = new X;

		p->m++;		// p and q can both be used like ptrs
		q->m++;

		// ... [ Here we might throw ] 

		delete q;   // <-- Oops! Resource never got released.
	}

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

If an exception is thrown in the `// ...` part, the object held by `p` is
correctly deleted by `auto_ptr`'s destructor while the `X` pointed to by
`q` is leaked. `auto_ptr` is a very lightweight class that we will get to
implement at some point. In particular, it is **not** a _reference
counted_ pointer. If you _copy_/_assign_ one `auto_ptr` into another 
(e.g., via the assignment operator:
`existing_auto_ptr = other_auto_pointer`), the one on the left hand side
of the assignment operator holds the pointer, whereas the one on the right
hand side holds 0. 

For example:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}

	#include<memory>
	#include<iostream>

	using std::cout;

	struct X {
		int m;
		// OTHER FIELDS AND FUNCTIONS
	};

	int main(){
		auto_ptr<X> p(new X);
		auto_ptr<X> q = p;   // <-- Copy constructor

		cout << "p " << p.get() << " q " << q.get() << "\n";
	}

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

should print a pointer followed by a non-0 pointer. 

For example:
    `p 0x0 q 0x378d0`

The member function `auto_ptr::get()` returns the pointer being held by
the `auto_ptr` object.

This is very closely related to an idiom something known as _move
semantics_, and it is different from the usual _copy semantics_. After
the midterm we will talk about it in more detail. For now, we'll content
ourselves knowing that there is a difference. In particular, you should
never use an `auto_ptr` as a member of a standard container (e.g.,
`std::vector` `std::deque`, etc.) . The standard containers require the
usual copy semantics.

For example the statement:
    `std::vector< auto_ptr<X> >v;	// Oops!`

is an error. The `auto_ptr` holds a pointer to an individual element,
not a pointer to an array:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}

	void some_other_function(int n) {
		auto_ptr<X> p(new X[n]);	// Oops again!
		// ...
	}

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

This is an error because the destructor will delete the pointer using 
delete rather than `delete[]` and will fail to invoke the destructor for
the last n-1 `X`s.

So, should we use an `auto_array` to hold arrays? The simple answer is, no.
Because ...

- There is no `auto_array` in c++ (there is no need for one), and
- we can use `std::vector`.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}

	void some_other_function(int n) {
		std::vector<X> v(n);
		// ...
	}

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Should an exception occur in the `...` part, `v`'s destructor will be
correctly invoked.
