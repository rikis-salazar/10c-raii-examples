## Why is it that `Qt` does not leak?

Suppose we wanted to code something along the lines of

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}

   int main(){
      // Heap objects... (we should be careful)
      Point2D* boring_pt = new Point2D(3,5);
      ColorPoint2D* pink_pt = new ColorPoint2D(2,4,"rosa");  // <-- Rose?

      // Stack object!
      PointsOnSomePlane points;  // <-- We'll figure this one out

      points.insert(boring_pt);
      points.insert(pink_pt);

      return 0;
   } // Oops. Leaks!!! Or, does it???

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Moreover, assume we run special software that tells us that our code
**DOES NOT** leak. What is going on here?


### A Stack object that _manages_ heap objects

To understand what is going on here, let us attempt to implement the
`PointsOnSomePlane` class.

First attempt:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}

class PointsOnSomePlane{
   private:
      // We assume all point2D 'live' on the heap, hence the 
      // vector of pointers instead of vector of points.
      std::vector<Point2D*> thePoints;

   public:
      PointsOnSomePlane() = default ;   // Let the compiler handle this one

      // More points please...
      void insert( Point2D* p );

      // Did you notice we are dealing with inheritance?
      // "Use virtual destructors..." is a good piece of advice. 
      virtual ~PointOnSomePlane();
};

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Ok. That's a good start. Let us define the destructor first. It should
be easy enough for us.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}

PointsOnSomePlane::~PointsOnSomePlane(){
   // Just move through the std::vector and call delete
   // on every pointer... After all, our points leave
   // on the heap.
   for ( auto& p : thePoints )  // <-- Why the &???
      delete p;

}

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Now, let us tackle the `insert()` member function.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}

void PointsOnSomePlane::insert( Point2D *p ){

   thePoints.push_back();

   return;
}

// So, what's the big deal? It is just one line.
// Or... Is it???

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

**Wait!!!** What happens if we _insert_ a point twice?

Second try:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}

void PointsOnSomePlane::insert( Point2D *p ){

   for ( auto& x : thePoints )
      // If point already there, do nothing.
      if ( x == p )
         return;

   thePoints.push_back();

   return;
}

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 


**Wait!!!** What happens if we _insert_ a null pointer?

Third try:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}

void PointsOnSomePlane::insert( Point2D *p ){

   // Do not add 'non-existent' points
   if ( p == nullptr )
      return;

   for ( auto& x : thePoints )
      // If point already there, do nothing.
      if ( x == p )
         return;

   thePoints.push_back();

   return;
}

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

_DONE. Yes!_ **Wait!** What if the `push_back` fails?

n-th try:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}

// void PointsOnSomePlane::insert( Point2D *p ) // <-- change return type
bool PointsOnSomePlane::insert( Point2D *p ){

   // Do not add 'non-existent' points
   if ( p == nullptr )
      return false;

   for ( auto& x : thePoints )
      // If point already there, do nothing.
      if ( x == p )
         return false;

   thePoints.push_back();

   return true;
}

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

**DONE!** As they say:

> "Half of programming is 99% of cases..."


